
TUI Stay - eCommerce

    Your challenge is to build a 'microsite' and/or API that provides a functional solution for travelers 
    to find the best and/or cheapest hotels available in the destinations specified below.
    In addition to providing visitors with a selection of offers for each destination, 
    we also want to provide information on the local climate &amp; weather.
    If you'd like to use APIs to retrieve offer and or weather data, 
    you could try the AccuWeather API (e.g. https://developer.accuweather.com ) and 
    a public hotels REST API (e.g. https://developers.amadeus.com/self-service/category/hotel/api-doc/hotel-search/api-reference ).

Functional requirements:
    - How to present the destinations, offers and the which data to include is completely up to you. 
    Do keep it mind the goal of the end-user, which is to find hotels in these destinations 
    and get an understanding of the weather / climate condition to expect. 
    - The microsite should contain at least 2 pages and provide enough and relevant data and functionalities 
    (e.g. ability to search, sort, filter or view more details) for the visitor to make an informed decision 
    - The available offers should be located in destinations in the following four countries: Portugal, Spain, Italy and Brazil.
    We value creativity and encourage you to create your own requirements while working on this assignment.
    Design requirements :
    - The look of the microsite is entirely up to you, as long as it is user friendly. 
    You can and should use our design system, TAGUS (https://zeroheight.com/191b8a909/p/1411cd-tagus) for reference. 

Front-end requirements:
    - Only ES6+ should be used or Typescript if you prefer (no jQuery)
    - CSS can be written using SASS (we prefer SASS) or LESS (no Bootstrap and BEM is a bonus)
    - Please use React or Vue (we prefer Vue)

Back-end requirements:
    - Please populate the data from the APIs of your choice into a non-relational database (we prefer MongoDB) 
    - create a REST API built with a node.js web framework (ex.: Express.js or other at your choice) to retrieve the data. 
    - We will focus on the architecture and technical excellence, 
    - REST API design and best practices, knowledge of node.js, security, performance and scalability for the backend side.

Full-stack requirements: 
    - Front-end + Back-end requirements 
    - Technical excellence
    - Attention to UI/UX
    - Attention to details while considering the business and user side
    - Framework guru
    - Accessibility
    - performance and security
    - Semantics and modularity

front-end and full-stack onlyBonus:
    - i18n
    - Unit testing (ex.: Jest)
    - SSR (Next or Nuxt) 
    - SEO * front-end and full-stack onlyTimeline   


## Instalation

### Using docker

`
$ docker-compose up --build -d
`

localhost:3000 => server side
localhost:8080 => client side


### Using npm

`
$ cd server 
$ npm run start:dev
`

localhost:3000

`
$ cd client 
$ npm run serve
`

localhost:8080